# **About me**
My name is Michael Kennedy and I'm User Experience Researcher, Design Thinking Facilitator. 
I'm Currently Principal User Experience Researcher at Veracode, where my teams work enables developers to deliver secure code. 


# **My role**
I see my role as follows:

- Illuminate the discovery process by enriching new innitiatives with context, narrative, and understanding grounded in research. 

- Maintaining the right atmosphere for iterative design research with support for decision making. Providing clarity around sample sizes and the meaning or interpretation of findings. 

- Create repeatable, transparent, and historical research artifacts that inspire confidence and impart insights effectively. Allowing the work of others to be measured accurately and against criteria which is of shared importance. 

- Most importantly, I work to creatively collaborate. I am here to make sure PMs, Dev, and design are not alone in our discovery or refinement processes and are supported with results that maintain or accellerate the momentum towards great product delivery as measured by users themselves.



# **Working with me**
I can maintain a high level of energy and attention towards a few larger efforts at once. I'm always someone to say yes to a  conversation about something new. But, I do prefer when a team process supports planning. I have often found myself creating team processes where the did not exist to support unplanned work as the appetite for UX work increases at an organization. 

I am a planner through and through, and almost enjoy packing as much as the trip. I love to talk about what could be done by a capable team, and I can help others to imagine better future states. When the rubber hits the road I often find that my planning has offered me an efficient path forward.

I have referred to myself as "Vanna Whiteboard" because I am happy to be the bouncing ball that focuses a conversation towards its outcomes and next steps. This has led me towards innovation and product roadmap strategy circles where UX has not typically been involved. I have used these opportunities to build more diverse, collaborative and cross-functional efforts in every role I've taken on. 

I am very generous and accomodating of my colleagues time, effort, and even emotion. As a naturally empathetic person, I appreciate and perpetuate kindness, and a healthy work environment and I do not thrive in uncertainty, doubt, or negativite atmospheres. I really like the research concept of "unconditional regard" and use this model to appreciate both my participants and colleagues.

I seek honest feedback an strive to exceed others expectations for the work we share together while also striving to deliver research work to the standards that research peers would appreciate. I push boundaries where I see opportunities to mix methods or answer questions more clearly, focusing my creativity on elegant planning. I take criticism very well, having cut my teeth in art school critique circles, and can deliver difficult criticism constructively. 

Lastly, I like to share success and celebrate acheivement with a diverse and cross-functional team. When we do great work together, we will surely know it and celebrate the acheivement because we will have measured the effect and impact and will know what it means for our organization. I wish all who read this that happiness!

# **Outside of work**
I live in Watertown, MA (EST/EDT) with my wife and two small long-haired chihuahuas. I enjoy artful hobbies like soapmaking, food science, and music, and I build treehouses with friends on a cooperative farm. 
